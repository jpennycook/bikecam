# extract from OSC
# copy directories from phone to D:\OSC\source
$OSCToolsDir = "C:\OSC"
$ZipPath = "C:\PortableApps\7-ZipPortable\App\7-Zip64\7z.exe"
$RootDir = "D:\OSC\source\files\OSV"
$ChildDirs = Get-ChildItem $RootDir -Filter "SEQ*"

Foreach ($SourceDir in $ChildDirs.Fullname) {
    Write-Verbose "Processing [$($SourceDir)]"
    #$SourceDir = "D:\OSC\source\files\OSV\SEQ_3a4b58d4-1eb0-4e69-98ea-ce0a4d3a871d"
    $SourceFile = (Get-ChildItem "$($SourceDir)" -Filter "*.mp4").fullname -join "|"
    Write-Verbose "Processing files [$($SourceFile)]"

    $TrackTxtFileDate = (get-item "$($SourceDir)\track.txt.gz").LastWriteTime.ToString("s")
    #$DestDir = Join-Path "D:\OSC\" ($TrackTxtFileDate -replace ":","")
    $DestDir = Join-Path "D:\OSC\output" "$($SourceDir.split("\")[-1])"

    (New-item -Path "$($DestDir)" -ItemType Directory -Force -ErrorAction SilentlyContinue) | out-null
    Write-Verbose "Extracting images from [$($SourceFile)]"
    ffmpeg -hide_banner -hwaccel auto -threads 1 -i "concat:$($SourceFile)" "$($DestDir)\%04d.jpg"
    Write-Verbose "Adding exif info from track.txt"
    & $ZipPath e "$($SourceDir)\track.txt.gz" -o"$($DestDir)" -y
    Push-Location $OSCToolsDir
    python osc_tools.py generate_exif -p "$($DestDir)" -l i
    Pop-Location
} # foreach
# & "$($ENV:OneDrive)\backups\mapillary_tools.exe" upload --advanced --import_path "E:\Users\user2\AppData\Local\mapillary\upload" --number_threads 8
#& "$($ENV:OneDrive)\backupS\mapillary_tools.exe" process_and_upload --advanced --import_path "D:\OSC\output" --number_threads 8 --user_name jpennycook --rerun --interpolate_directions --overwrite_all_EXIF_tags
& "$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools"  process_and_upload --advanced --import_path "D:\OSC\output" --number_threads 8 --user_name jpennycook --rerun --interpolate_directions --overwrite_all_EXIF_tags