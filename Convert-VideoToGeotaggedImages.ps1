<#
.SYNOPSIS

Converts action camera video to geotagged images suitable for upload to Mapillary

.DESCRIPTION

Takes the specified video files and a GPX file, and uses mapillary_tools to convert them to images.
The files will be tagged so they can be uploaded to Mapillary.  Requires ffmpeg and mapillary_tools.

.PARAMETER Path
Specifies the action camera videos - assumed to be MP4 or MOV.

.PARAMETER Destination
Specifies the output directory to contain the generated images

.PARAMETER GpxPath
Specifies the GPX file to use

.PARAMETER Offset
Specifies the number of seconds that the video is behind position

.PARAMETER OffsetAngle
Specifies the offset angle (compared to one from the GPX file) - usually 0 for front-facing camera, or 180 for rear-facing camera

.PARAMETER Camera
The action camera used to generate the video.  Currently limited to Currently limited to "CycliqFly12CE", "CycliqFly12Sport", "CycliqFly6CEGen2", "OLFI", "CrossTour-CT9900", "CycliqFly6CEGen3"

.PARAMETER Hibernate
Specify to hibernate when complete

.PARAMETER VideoSampleInterval
Specifies the period in seconds between image samples

.PARAMETER MapillaryTools
Specifies the location of mapillary_tools.exe

.PARAMETER MapillaryUserName
Specifies the username to use with Mapillary

.EXAMPLE

PS> .\Convert-VideoToGeotaggedImages.ps1 -Path "$($ENV:USERPROFILE)\bikecam-crop\CycliqFront\20201202a" -Offset -0.5 -Camera "CycliqFly6CEGen2" -GPXPath "$($ENV:USERPROFILE)\OneDrive\bikeAuto_Track__Fri_3rd_May_2013__002.gpx"
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory=$true)]
    [ValidateNotNullOrEmpty()]
    [string[]]$Path, # videos
    [string]$Destination,
    [Parameter(Mandatory=$true)]
    [System.IO.FileInfo]$GPXPath, # GPX file
    [single]$OffSet = 0, # seconds that the video is behind position
    [int]$OffSetAngle, # 0 for front-facing, 180 for rear-facing
    [ValidateSet("CycliqFly12CE", "CycliqFly12Sport", "CycliqFly6CEGen2", "OLFI", "CrossTour-CT9900", "CycliqFly6CEGen3")]
    [string]$Camera,
    [switch]$Hibernate,
    [single]$VideoSampleInterval= 1, # 1 for Locus or OSMTracker, 2-5 for Endomondo
    [System.IO.FileInfo]$MapillaryTools = "$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools.exe",
    [string]$MapillaryUserName = "jpennycook"
) # param
$OldErrorActionPreference = $ErrorActionPreference
$ErrorActionPreference = "Stop"
$unixEpochStart = new-object DateTime 1970,1,1,0,0,0,([DateTimeKind]::Utc) # need to specify start time as number of milliseconds since UNIX epoch start
If ( -not $Camera ) {
    Try {
    $Camera = (Get-Item (Resolve-Path $Path)).Parent.Name
    } # try
    Catch {
        Write-Warning "Failed to determine camera type from path [$($Path)]"
        Throw $_
    } # catch
} # if
switch ($Camera) {
    "CycliqFly12CE" {
        [string]$Make = "Cycliq"
        [string]$Model = "Fly12 CE"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 0
        } # if
    } # CycliqFly12CE
    "CycliqFly12Sport" {
        [string]$Make = "Cycliq"
        [string]$Model = "Fly12 Sport"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 0
        } # if
    } # CycliqFly12Sport
    "CycliqFly6CEGen2" {
        [string]$Make = "Cycliq"
        [string]$Model = "Fly6 CE Gen 2"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 180
        } # if
    } # CycliqFly6CEGen2
    "CycliqFly6CEGen3" {
        [string]$Make = "Cycliq"
        [string]$Model = "Fly6 CE Gen 3"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 180
        } # if
    } # CycliqFly6CEGen2
    "OLFI" {
        [string]$Make = "OLFI"
        [string]$Model = "ONE.FIVE BLACK 2nd Generation"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 0
        } # if
    } # OLFI
    "CrossTour" {
        [string]$Make = "CrossTour"
        [string]$Model = "CT9900"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 0
        } # if
    } # OLFI
    Default {
        [string]$Make = "NA"
        [string]$Model = "NA"
        If (-not $PSBoundParameters.ContainsKey('OffsetAngle') ) {
            [int]$OffSetAngle = 0
        } # if
    } # default
}# switch
$PathInfo = ( Get-Item (Resolve-Path $Path -ErrorAction:Stop) -ErrorAction:Stop )
If ( $PathInfo -is [System.IO.DirectoryInfo] ) {
    $FilesToProcess = Get-ChildItem -Path $PathInfo -File # if it's a directory, get all the files
} else {
    $FilesToProcess = $PathInfo # otherwise, it must be (a list of) files
} # if
foreach ($FileToProcess in $FilesToProcess) {
    Write-Output "Processing: [$($FileToProcess.BaseName)]"
    $ParentDir = $FileToProcess.Directory.Name
    If (-not $Destination) {    
        [string]$Destination = Join-Path "$($ENV:LOCALAPPDATA)" "mapillary\$($ParentDir)"
    } # if
    ( New-Item -Path $Destination -ItemType Directory -Force ) | out-null
    $DateTimeString = $FileToProcess.Basename
    $Year = $DateTimeString.Substring(0,4)
    # _
    $Month = $DateTimeString.Substring(5,2)
    $Day = $DateTimeString.Substring(7,2)
    # _
    $Hour = ($DateTimeString.Substring(10,2))
    $Minute = $DateTimeString.Substring(12,2)
    $Second = ($DateTimeString.Substring(14,2))
    $WindowsDate = (Get-Date -Year $Year -Month $Month -Day $Day -Hour $Hour -Minute $Minute -Second $Second).Addmilliseconds(-$Offset*1000)
    
    #$UnixDate = [int64](($WindowsDate.ToUniversalTime() - $unixEpochStart).totalmilliseconds) # milliseconds since UNIX epoch start
    $UnixDate = [int64](($WindowsDate - $unixEpochStart).totalmilliseconds) # milliseconds since UNIX epoch start - Windows date is UTC because it is based on Media Encoded Date in UTC, and GPX is UTC anyway
    Write-Verbose "Windows: $($WindowsDate)"
    Write-Verbose "UNIX: $($UNIXDate)"

    # mapillary_tools video_process --import_path "path/to/images" --video_import_path "path/to/videos" --user_name "mapillary_user" --advanced --geotag_source "gopro_videos" --geotag_source_path "path/to/videos" --interpolate_directions --video_sample_interval 0.5 --overwrite_EXIF_gps_tag
    # if no direction data: --interpolate_directions
    (& $MapillaryTools video_process --video_import_path "$($FileToProcess.FullName)" --advanced --geotag_source "gpx" --geotag_source_path "$($GPXPath)" --user_name "$($MapillaryUserName)" --offset_time "0" --offset_angle $OffSetAngle --video_start_time "$($UNIXDate)" --import_path "$($Destination)" --device_make "$($Make)" --device_model "$($Model)" --overwrite_all_EXIF_tags --rerun --video_sample_interval $VideoSampleInterval) | out-null
    Write-Output "Completed: [$($FileToProcess.BaseName)]"
} # foreach
$ErrorActionPreference = $OldErrorActionPreference
If ($Hibernate) {
    Write-Verbose "$(Get-Date): Hibernating the computer"
    shutdown /h
} # if