<#
.SYNOPSIS

Copy video files from an SD card

.DESCRIPTION

Copies video files of type mp4 and mov from an SD card to a directory in your user profile.  Currently supports Cycliq Fly12 CE, Cycliq Fly12 Sport, Cycliq Fly 6 CE GEN2, CrossTour CT9900, OLFI

.PARAMETER Path
Specifies the location of the SD card, T:\ by default

.PARAMETER Destination
Specifies the output directory to contain the copied files

.PARAMETER Passthru
Sends command output to screen

.EXAMPLE

PS> .\Copy-BikeCam.ps1 -Path "T:\" -Destination "$($ENV:USERPROFILE)\bikecam-new" -PassThru
#>

[CmdletBinding()]
param (
	$FileType = '(mp4|mov)',
	[alias("IntermediateDir")]
	$TempDir = "$($Env:Temp)\bikecam-new\",
	[alias("DestDir")]
	$Destination = "$($ENV:USERPROFILE)\bikecam-new",
	[System.IO.DirectoryInfo]$Path = "T:\",
	[switch]$Passthru
) # param

#region functions
function Copy-JPVideoFromIntermediateToDestination {
	param (
		[alias("IntermediateDir")]
        [Parameter(Mandatory)][string]$Path,
		[alias("DestDir")]
		[Parameter(Mandatory)][string]$Destination,
		[Parameter(Mandatory)][string]$FileType,
		[switch]$Passthru
    ) # param
	$Files = ( Get-ChildItem $Path -File ).where({$_.Extension -match $FileType})
	If ( $Files ) {
		Push-Location $Path
		$Date = ( ( $Files.CreationTimeUtc).ForEach({$_.ToString('yyyyMMdd')}) | Select-Object -Unique)
		Foreach ( $Dir in $Date ) {
			New-item -Path $Destination -Name "$($Dir)" -ItemType Directory -Force
			$SourceFiles = $files.where({$_.CreationTimeUTc.ToString('yyyyMMdd') -eq $Dir })
			Foreach ( $SourceFile in $SourceFiles ) {
				Move-Item "$($IntermediateDir)\$($SourceFile.Name)" "$($Destination)\$($Dir.replace('_',''))\$($SourceFile.Name)" -PassThru:$Passthru -Force
			} # foreach
		} # foreach
		Pop-Location
	} # if
} # function Copy-JPVideoFromIntermediateToDestination
#endregion functions

# Cycliq Fly 12 CE front camera
[string]$Name = "CycliqFly12CE"
$CameraDir = Join-Path -Path $Path -ChildPath "DCIM"
$SourceDir = ( Get-ChildItem -Path $CameraDir -Directory ).where({$_.Name -match '\d{3}_CF12'}).fullname
If ($SourceDir) {
	$IntermediateDir = Join-Path -Path $TempDir -ChildPath $Name
	New-item -Path "$($IntermediateDir)" -ItemType Directory -Force
	$DestDir = Join-Path -Path $Destination -ChildPath $Name
	foreach ( $Dir in $SourceDir) {
		# copy files from SD card to intermediate location
		$CmdArgs = @(
			"$($Dir)"
			"$($IntermediateDir)"
			"/FFT"
			"/MT"
			"/M"
			"/ETA"
			"/R:2"
			"/MIN:5000000"
		) # CmdArgs
		If ($PassThru) {
			robocopy @CmdArgs
		} else {
			robocopy @CmdArgs | Out-Null
		} # if passthru
		# Move files to directories based on date
		# Will overwrite files if names match - unlikely as 101_RIDE is created once filenames get to CYQ_0999, and start again at CYQ_0001
	} # foreach
	Copy-JPVideoFromIntermediateToDestination -Path $IntermediateDir -Destination $DestDir -Passthru -FileType $FileType
} # if

# Cycliq Fly 12 Sport front camera, also detects Cycliq Fly 6 CE GEN 3 rear camera unfortunately!
[string]$Name = "CycliqFly12Sport"
$CameraDir = Join-Path -Path $Path -ChildPath "DCIM"
$SourceDir = ( Get-ChildItem -Path $CameraDir -Directory ).where({$_.Name -match '\d{3}_RIDE'}).fullname
If ($SourceDir) {
	$IntermediateDir = Join-Path -Path $TempDir -ChildPath $Name
	New-item -Path "$($IntermediateDir)" -ItemType Directory -Force
	$DestDir = Join-Path -Path $Destination -ChildPath $Name
	foreach ( $Dir in $SourceDir) {
		# copy files from SD card to intermediate location
		$CmdArgs = @(
			"$($Dir)"
			"$($IntermediateDir)"
			"/FFT"
			"/MT"
			"/M"
			"/ETA"
			"/R:2"
			"/MIN:5000000"
		) # CmdArgs
		If ($PassThru) {
			robocopy @CmdArgs
		} else {
			robocopy @CmdArgs | Out-Null
		} # if passthru
		# Move files to directories based on date
		# Will overwrite files if names match - unlikely as 101_CF12 is created once filenames get to CYQ_0999, and start again at CYQ_0001
	} # foreach
	Copy-JPVideoFromIntermediateToDestination -Path $IntermediateDir -Destination $DestDir -Passthru -FileType $FileType
} # if

# CrossTour helmet camera
[string]$Name = "CrossTour-CT9900"
$CameraDir = $Path
$SourceDir = ( Get-ChildItem -Path $CameraDir -Directory ).where({$_.Name -match '^VIDEO$'}).fullname
If ($SourceDir) {
	$IntermediateDir = Join-Path -Path $TempDir -ChildPath $Name
	New-item -Path "$($IntermediateDir)" -ItemType Directory -Force
	$DestDir = Join-Path -Path $Destination -ChildPath $Name
	foreach ( $Dir in $SourceDir) {
		# copy files from SD card to intermediate location
		$CmdArgs = @(
			"$($Dir)"
			"$($IntermediateDir)"
			"/FFT"
			"/MT"
			"/M"
			"/ETA"
			"/R:2"
			"/MIN:5000000"
		) # CmdArgs
		If ($PassThru) {
			robocopy @CmdArgs
		} else {
			robocopy @CmdArgs | Out-Null
		} # if passthru
		# Move files to directories based on date
		# Will overwrite files if names match
	} # foreach
	Copy-JPVideoFromIntermediateToDestination -Path $IntermediateDir -Destination $DestDir -Passthru:$Passthru -FileType $FileType
} # if

#Cycliq Fly6 CE GEN 2 rear camera
[string]$Name = "CycliqFly6CEGen2"
$CameraDir = Join-Path -Path $Path -ChildPath "DCIM"
$SourceDir = ( Get-ChildItem -Path $CameraDir -Directory ).where({$_.Name -match '\d{3}_FLY6'}).fullname
If ($SourceDir) {
	$IntermediateDir = Join-Path -Path $TempDir -ChildPath $Name
	New-item -Path "$($IntermediateDir)" -ItemType Directory -Force
	$DestDir = Join-Path -Path $Destination -ChildPath $Name
	foreach ( $Dir in $SourceDir) {
		# copy files from SD card to intermediate location
		$CmdArgs = @(
			"$($Dir)"
			"$($IntermediateDir)"
			"/FFT"
			"/MT"
			"/M"
			"/ETA"
			"/R:2"
			"/MIN:5000000"
		) # CmdArgs
		If ($PassThru) {
			robocopy @CmdArgs
		} else {
			robocopy @CmdArgs | Out-Null
		} # if passthru
		# Move files to directories based on date
		# Will overwrite files if names match
	} # foreach
	Copy-JPVideoFromIntermediateToDestination -Path $IntermediateDir -Destination $DestDir -Passthru:$Passthru -FileType $FileType
} # if

#OLFI helmet camera
[string]$Name = "OLFI"
$CameraDir = Join-Path -Path $Path -ChildPath "DCIM"
$SourceDir = ( Get-ChildItem -Path $CameraDir -Directory ).where({$_.Name -match '^Movie$'}).fullname
If ($SourceDir) {
	$IntermediateDir = Join-Path -Path $TempDir -ChildPath $Name
	New-item -Path "$($IntermediateDir)" -ItemType Directory -Force
	$DestDir = Join-Path -Path $Destination -ChildPath $Name
	foreach ( $Dir in $SourceDir) {
		# copy files from SD card to intermediate location
		$CmdArgs = @(
			"$($Dir)"
			"$($IntermediateDir)"
			"/FFT"
			"/MT"
			"/M"
			"/ETA"
			"/R:2"
			"/MIN:5000000"
		) # CmdArgs
		If ($PassThru) {
			robocopy @CmdArgs
		} else {
			robocopy @CmdArgs | Out-Null
		} # if passthru
		# Move files to directories based on date
		# Will overwrite files if names match
	} # foreach
	$Files = ( get-childitem $IntermediateDir -File ).where({$_.Extension -match $FileType})
	If ( $Files ) {
		Push-Location $IntermediateDir
		$DateFromName = ( $files.name.substring(0,9) | Select-Object -Unique)
		Foreach ( $Dir in $DateFromName ) {
			New-item -Path $DestDir -Name "$($Dir.replace('_',''))" -ItemType Directory -Force
			$SourceFiles = ( Get-ChildItem -path $IntermediateDir -Filter "$($Dir)*" -File )
			Foreach ( $SourceFile in $SourceFiles ) {
				Move-Item "$($IntermediateDir)\$($SourceFile.Name)" "$($DestDir)\$($Dir.replace('_',''))\$($SourceFile.Name)" -PassThru:$Passthru
			} # foreach
		} # foreach
		Pop-Location
	} # if files
} # if sourcedir
