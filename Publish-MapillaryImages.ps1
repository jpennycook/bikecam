<#
.SYNOPSIS

Uploads to Mapillary

.DESCRIPTION

Uploads the specified directory of geotagged images to Mapillary.  Usually a directory (e.g. "20220101"), which contains a "mapillary_sampled_video_frames" subdirectory, and then at least one subdirectory per converted video file.  
Requires mapillary_tools.

.PARAMETER Path
The directory containing the geotagged images, or a parent of that directory.

.PARAMETER Hibernate
Specify to hibernate when complete

.PARAMETER MapillaryTools
Specifies the location of mapillary_tools.exe

.PARAMETER MapillaryUserName
Specifies the username to use with Mapillary.  Not normally required, sometimes useful when reuploading due to a change in mapillary_tools version or a backend (website) change.

.PARAMETER Delay
The delay in seconds between uploading directories.  30 seconds seemed reasonable when Mapillary were overloaded.

.EXAMPLE

PS> .\Publish-MapillaryImages.ps1

Uploads from "$($ENV:LOCALAPPDATA)" "mapillary\upload"

.EXAMPLE

PS> .\Publish-MapillaryImages.ps1 -Path c:\Mapillary\Upload

Uploads from c:\Mapillary\Upload
#>

[CmdletBinding()]
Param (
    [System.IO.DirectoryInfo]$Path = ( Join-Path "$($ENV:LOCALAPPDATA)" "mapillary\upload" ),
    [System.IO.FileInfo]$MapillaryTools = "$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools.exe",
    [switch]$Force,
    [switch]$Hibernate,
    [string]$MapillaryUserName,
    [int]$Delay = 30
) # param
If (-not (Test-Path -Path $Path -ErrorAction:SilentlyContinue)) {
    [string]$Message = "Directory [$($Path)] not found, cannot continue"
    Throw $Message
} # if
$Dirs = ( Get-ChildItem $Path -Directory).fullname
If (-not $Dirs) {
    [string]$Message = "Directory [$($Dirs)] does not have subdirectories, will use this directory only"
    Write-Verbose $Message
    $Dirs = ( Get-Item -Path $Path).fullname
} # if
If ( -not $Force ) {
    [string]$Message = "-Force not specified, normal upload, skip already uploaded"
} else {
    [string]$Message = "-Force specified, will reprocess and reupload even if already processed, may not achieve anything if images previously uploaded (wait until they are deleted from the website)"
} # if not force
Write-Verbose $Message
foreach ($dir in $dirs) {
    If ((Get-ChildItem -LiteralPath $Dir -Recurse -File -Force | Select-Object -First 1).Count -ne 0) {
        [string]$Message = "Files found in [$($Dir)], starting to upload"
        Write-Verbose $Message
        $Date = Get-Date (Get-Date).ToUniversalTime() -UFormat '+%Y-%m-%dT%H%M%S.000Z'
        $StdErrPath = "$($ENV:TEMP)\stderr-$(($dir -split '\\')[-1])-$($Date)"
        $CmdArgs = @{
            FilePath = $MapillaryTools
            ArgumentList = "upload --advanced --import_path $Dir --number_threads 16 --verbose"
            NoNewWindow = $true
            RedirectStandardError = $StdErrPath
            Wait = $True
        } # CmdArgs
        If ( $Force ) {
            $CmdArgs.ArgumentList = "process_and_upload --advanced --import_path $Dir --number_threads 16 --rerun --verbose"
        } #if not force
        If ($MapillaryUserName) {
            $CmdArgs.ArgumentList+= " --user_name $($MapillaryUserName)"
        } # if
        Start-Process @CmdArgs
        Start-Sleep -Seconds $Delay
    } else {
        [string]$Message = "Directory [$($Dir)] appears to be empty, skipping"
        Write-Verbose $Message
    } # if
} # foreach
If ($Hibernate) {
    Write-Verbose "$(Get-Date): Hibernating the computer"
    shutdown /h
} # if