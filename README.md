# bikecam

## Intro

Copy video files from bike cameras, crop them to remove the on-screen timestamp, extract geotagged images, then submit the geotagged images to Mapillary

## Pre-reqs

- `Crop-Timestamp.ps1`: Download EXIF Tool, ensure `c:\exiftool\exiftool.exe` is present (can change path, for example `.\Crop-TimeStamp.ps1 -ExifTool:"d:\exiftool\exiftool.exe"`)
- `Crop-Timestamp.ps1`: Download ffmpeg, ensure `c:\ffmpeg\bin\ffmpeg.exe` is present (can change path, for example `.\Crop-TimeStamp.ps1 -Ffmpeg:"c:\ffmpeg\bin\ffmpeg.exe"`)
- `Convert-Video.ps1`: ffmpeg.exe must be in the PATH
- `Convert-Video.ps1`: Download mapillary_tools.exe, ensure `$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools.exe` is present (can change path, for example `.\Convert-Video.ps1.ps1 -MapillaryTools:"c:\mapillary\mapillary_tools.exe`)
- `Crop-Timestamp.ps1`: Download mapillary_tools.exe, ensure `$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools.exe` is present (can change path, for example `.\Crop-Timestamp.ps1 -MapillaryTools:"c:\mapillary\mapillary_tools.exe`)
- `Publish-MapillaryImages.ps1`: Download mapillary_tools.exe, ensure `$($ENV:USERPROFILE)\AppData\Roaming\Mapillary Uploader\mapillary_tools.exe` is present (can change path, for example `.\Crop-Timestamp.ps1 -MapillaryTools:"c:\mapillary\mapillary_tools.exe`)

## Usage

- Copy the files from SD card to `$($ENV:USERPROFILE)\bikecam-new`, using `.\Copy-BikeCam.ps1`
- Remove any video files that are obviously not needed
- Crop the timestamps from the videos using `.\Crop-Timestamp.ps1`, specifying the (integer) number of seconds that the camera timestamp (OLFI) or Media Encoded Date (Cycliq - found in the EXIF data, e.g. using [MediaInfo](https://mediaarea.net/en/MediaInfo)) - is ahead of the time in the GPX file: UTC (Locus) or phone time (Endomondo), for example:-

  ``` PowerShell
  .\Crop-Timestamp.ps1 -Path "$($ENV:USERPROFILE)\bikecam\CycliqFront\20201202a" -Offset -11 -Camera "CycliqFront"
  ```

- Convert the videos to geotagged images using `.\Convert-Video.ps1`, fine-tuning the offset using a decimal.  Add -OffsetAngle 180 for a rear-facing camera. For example:-

  ``` PowerShell
  .\Convert-Video.ps1 -Path "$($ENV:USERPROFILE)\bikecam-crop\CycliqFront\20201202a" -Offset -0.5 -Camera "CycliqFront" -GPXPath "$($ENV:USERPROFILE)\OneDrive\bikeAuto_Track__Fri_3rd_May_2013__002.gpx"
  ```

- Delete any unwanted images
- Upload to Mapillary using `.\Publish-MapillaryImages.ps1 -Path "$($ENV:LOCALAPPDATA)\mapillary\upload" )`
