<#
.SYNOPSIS

Crops the timestamp from the bottom of a directory of videos

.DESCRIPTION

Scans the specified directory for videos from an action camera, then removes the bottom part of each which probably contains a timestamp.
A different directory is used to write the cropped videos.  Requires ffmpeg and exiftool.

.PARAMETER Path
Specifies the directory containing the action camera videos - assumed to be MP4.

.PARAMETER Destination
Specifies the output directory to contain the processed videos

.PARAMETER Offset
Specifies the number of seconds that the camera timestamp (OLFI,CrossTour) or Media Encoded Date (Cycliq) is ahead of the GPX file - UTC (Locus) or phone time (Endomondo)

.PARAMETER Camera
The action camera used to generate the video.  Currently limited to "CycliqFly12CE", "CycliqFly12Sport", "CycliqFly6CEGen2", "OLFI", "CrossTour-CT9900", "CycliqFly6CEGen3"

.PARAMETER CropFromBottom
Use to override the default for the camera type - the number of pixels to crop from the bottom of the video

.PARAMETER SkipNVidiaHwAccel
Specify to avoid the use of NVIdia-specific hardware acceleration - h264_cuvid and h264_nvenc

.PARAMETER Hibernate
Specify to hibernate when complete

.PARAMETER Ffmpeg
Specifies the location of ffmpeg.exe

.PARAMETER ExifTool
Specifies the location of exiftool.exe

.EXAMPLE

PS> .\Remove-TimeStamp.ps1 -Path "$($ENV:USERPROFILE)\bikecam\CycliqFront\20201202a" -Offset -11 -Camera "CycliqFly12Sport"
#>

[CmdletBinding()]
param (
    [Parameter(Mandatory=$true)]
    [System.IO.DirectoryInfo]$Path, # directory containing videos
    [System.IO.DirectoryInfo]$Destination = "$($Path -replace "bikecam","bikecam-crop")", # output directory
    [int]$OffSet = 0, # seconds that the camera timestamp (OLFI) or Media Encoded Date (Cycliq) is ahead of UTC (Locus) or phone time (Endomondo)
    [ValidateSet("CycliqFly12CE", "CycliqFly12Sport", "CycliqFly6CEGen2", "OLFI", "CrossTour-CT9900", "CycliqFly6CEGen3")]
    [string]$Camera,
    [int]$CropFromBottom, # number of pixels to crop from bottom
    [switch]$SkipNVidiaHwAccel, # specify to avoid the use of NVIdia-specific hardware acceleration - h264_cuvid and h264_nvenc
    [switch]$Hibernate,
    [System.IO.FileInfo]$Ffmpeg = "c:\ffmpeg\bin\ffmpeg.exe",
    [System.IO.FileInfo]$ExifTool = "c:\exiftool\exiftool.exe"
) # param
$OldErrorAction = $ErrorActionPreference
$ErrorActionPreference = 'Stop'
If ( -not $Camera ) {
    Try {
    $Camera = (Get-Item (Resolve-Path $Path)).Parent.Name
    } # try
    Catch {
        Write-Warning "Failed to determine camera type from path [$($Path)]"
        Throw $_
    } # catch
} # if
If ( -not $CropFromBottom ){
    switch ($Camera) {
        { $_ -in "CycliqFly12CE" } {
            [int]$CropFromBottom = 48
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromBottom):x=0:y=0"
            [string]$h264CropFilter = "0x$($CropFromBottom)x0x0"
        }
        { $_ -in "CycliqFly12Sport" } {
            [int]$CropFromBottom = 32
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromBottom):x=0:y=0"
            [string]$h264CropFilter = "0x$($CropFromBottom)x0x0"
        }
        { $_ -in "CycliqFly6CEGen2" } {
            [int]$CropFromBottom = 100
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromBottom):x=0:y=0"
            [string]$h264CropFilter = "0x$($CropFromBottom)x0x0"
        }
        { $_ -in "CycliqFly6CEGen3" } {
            [int]$CropFromBottom = 48
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromBottom):x=0:y=0"
            [string]$h264CropFilter = "0x$($CropFromBottom)x0x0"
        }
        { $_ -in "OLFI" } {
            [int]$CropFromBottom = 80
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromBottom):x=0:y=0"
            [string]$h264CropFilter = "0x$($CropFromBottom)x0x0"
        }
        { $_ -in "CrossTour-CT9900" } {
            [int]$CropFromTop = 48
            [string]$x264CropFilter = "crop=out_w=in_w:out_h=in_h-$($CropFromTop):x=0:y=$($CropFromTop)"
            [string]$h264CropFilter = "$($CropFromTop)x0x0x0"
        }
        Default {
            [string]$CropFromBottom = $null
        }
    } # switch camera    
} # if
switch ($Camera) {
    { $_ -in "CycliqFly12CE" } {
        [string]$FileSuffix = "C12CE"
    }
    { $_ -in "CycliqFly12Sport" } {
        [string]$FileSuffix = "C12S"
    }
    { $_ -in "CycliqFly6CEGen2" } {
        [string]$FileSuffix = "C6CEG2"
    }
    { $_ -in "CycliqFly6CEGen3" } {
        [string]$FileSuffix = "C6CEG3"
    }
    "OLFI" {
        [string]$FileSuffix = "OL"
    }
    "CrossTour" {
        [string]$FileSuffix = "CT"
    }
    Default {
        [string]$FileSuffix = "NA"
    }
} # switch camera
Write-Verbose "Camera: [$($Camera)]; suffix: [$($FileSuffix)]"
[string[]]$LaptopMfr = @("LENOVO") # skip NVIDIA hardware acceleration for this PC manufacturer
$Manufacturer = (Get-CimInstance -ClassName Win32_SystemEnclosure).Manufacturer
If ($Manufacturer -in $LaptopMfr) {
    Write-Verbose "Computer manufacturer is in list to skip using NVidia hardware acceleration, will use dxva2"
    [bool]$SkipNVidiaHwAccel = $true
} # if
( New-item -Path "$($Destination)" -ItemType Directory -Force -ErrorAction SilentlyContinue ) | out-null
Try {
    Write-Verbose "Parsing video files in [$($Path)]"
    ($Output = & $ExifTool $Path -json) 2>&1 | Out-Null # Get the exif data for each file in the directory, output as JSON.  Suppress writing status to STDERR, but captures both STDOUT and STDERR in variable
    $ExifData = $Output.Where({ $_ -isnot [System.Management.Automation.ErrorRecord]}) # remove the STDERR output
    $MetaData = ConvertFrom-Json -InputObject ($ExifData -join "`n") -ErrorAction:Stop
} # try
Catch {
    $ErrorActionPreference = $OldErrorAction
    Write-Error "unable to parse videos in [$($Path)], error $_"
} #catch
Foreach ($File in $MetaData) {
    If ( $File.FileName -and $File.MediaCreateDate ) {
        try {
            $SourcePath = ( Join-Path -Path $Path -ChildPath $File.FileName )
            [string]$DateTimeString = $File.MediaCreateDate
            Write-verbose "Processing [$($File.FileName)]"
            switch ($Camera) {
                ##################################################################
                # CrossTour-CT9900 records videos in China timezone (+8 hours)
                ##################################################################
                { $_ -in "CycliqFly12CE" } {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = '00','00','00' # Cycliq Fly12 CE sets Media Encoded Date to start of video
                }
                { $_ -in "CycliqFly12Sport" } {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = $File.MediaDuration -split ":" # Cycliq Fly12 Sport sets Media Encoded Date to end of video
                }
                { $_ -in "CycliqFly6CEGen2","CycliqFly6CEGen3" } {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = '00','00','00' # Cycliq Fly6 CE sets Media Encoded Date to start of video
                }
                { $_ -in "CrossTour" } {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = '00','00','00' # CrossTour CT9900 sets Media Encoded Date to start of video (but 8 hours before visible timestamp)
                }
                { $_ -in "OLFI" } {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = $File.MediaDuration -split ":" # OLFI sets Media Encoded Date to end of video
                }
                Default {
                    $VideoDurationHour, $VideoDurationMinute, $VideoDurationSecond = '00','00','00'
                }
            } # switch camera
            # Get the date and time from the metadata
            $Year = $DateTimeString.Substring(0,4)
            # :
            $Month = $DateTimeString.Substring(5,2)
            # :
            $Day = $DateTimeString.Substring(8,2)
            # <space>
            $Hour = ($DateTimeString.Substring(11,2))
            # :
            $Minute = $DateTimeString.Substring(14,2)
            # :
            $Second = ($DateTimeString.Substring(17,2))
            $VideoCameraDate = (Get-Date -Year $Year -Month $Month -Day $Day -Hour $Hour -Minute $Minute -Second $Second).AddHours(-$VideoDurationHour).AddMinutes(-$VideoDurationMinute).AddSeconds(-$VideoDurationSecond) # the date/time reported by the camera
            $WindowsDate = $VideoCameraDate.Addseconds(-$Offset) # the actual date to match the GPX file
            switch ($Camera) {
                { $_ -in "Cycliq", "CycliqFront", "CycliqRear" } {
                    $WindowsDate = $WindowsDate.ToUniversalTime() # Cycliq timestamp and Media Encoded Date are local time, not UTC
                }
                "OLFI" {
                    $WindowsDate = $WindowsDate.ToUniversalTime() # OLFI timestamp and Media Encoded Date are local time, not UTC
                }
                "CrossTour" {
                    $WindowsDate = $WindowsDate.AddHours(8) # CrossTour Media Encoded Date is 8 hours behind visible timestamp, and appears to be UTC
                }
                Default {
                    # assume timestamp is local time, Media Encoded Date appears to not use summer time
                }
            } # switch camera
            $DestYear = ([string]$WindowsDate.Year).PadLeft(4,'0')
            $DestMonth = ([string]$WindowsDate.Month).PadLeft(2,'0')
            $DestDay = ([string]$WindowsDate.Day).PadLeft(2,'0')
            $DestHour = ([string]$WindowsDate.Hour).PadLeft(2,'0')
            $DestMinute = ([string]$WindowsDate.Minute).PadLeft(2,'0')
            $DestSecond = ([string]$WindowsDate.Second).PadLeft(2,'0')
            $FileBaseName = $File.Filename.Split('.',2)[0]
            $DestSuffix = "$($FileSuffix)$($FileBaseName.Substring($FileBaseName.LastIndexOf('_')+1))"
            $DestinationFileName = "$($DestYear)_$($DestMonth)$($DestDay)_$($DestHour)$($DestMinute)$($DestSecond)_$($DestSuffix).MP4"
            $DestinationPath = (Join-Path -Path $Destination -ChildPath $DestinationFileName)
        } # try
        Catch {
            Write-Verbose "Problem pre-processing file"
            Write-Warning $_
        } # Catch
        Try {
            Write-Verbose "Creating [$($DestinationFileName)] by cropping [$($File.FileName)]"
            If ($SkipNVidiaHwAccel) {
                Write-Verbose "Using dxva2 acceleration with libx264"
                & $Ffmpeg -n  -hide_banner -loglevel error -stats -hwaccel dxva2 -i "$($SourcePath)" -filter:v $x264CropFilter -c:v libx264 -preset veryfast -crf 16 -b:v 8M "$($DestinationPath)"
            } else {
                Write-Verbose "Using NVidia (cuvid) acceleration with h264_cuvid and h264_nvenc"
                & $Ffmpeg -y -loglevel error -stats -hwaccel cuvid -c:v h264_cuvid -crop $h264CropFilter -i "$($SourcePath)" -c:v h264_nvenc -rc 32 -preset p5 -tune 4 -b:v 30M "$($DestinationPath)"
            } # if SkipNVidiaHwAccel
        } # try
        Catch {
            # Note - does not pick up ffpmeg errors!
            Write-Verbose "Problem cropping file"
            Write-Warning $_
        } # Catch
    } #if
} # Foreach
If ($Hibernate) {
    Write-Verbose "$(Get-Date): Hibernating the computer"
    shutdown /h
} # if
$ErrorActionPreference = $OldErrorAction