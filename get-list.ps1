
Param (
    [System.IO.DirectoryInfo]$Path # Directory containing the cropped videos
) # Param
[CmdletBinding()]
$Files = ( Get-ChildItem -Path (Join-Path "$Path" "*.mp4" ) )
$Prefix = '"'
[string]$string = ""
$Suffix = '"'
foreach ($File in $Files.Name ){
    $String+= "$($Prefix)$($File)$($Suffix)`n"
}# foreach
Write-Output $String